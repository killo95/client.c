#include <stdio.h>
#include <readline/history.h>
#include <readline/readline.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>
#include <netdb.h>
#include <unistd.h>
#include <curses.h>
#include <term.h>

void menu();
char get_choice();
void download_file(int s);
void list_files(int s);
void close_connection(int s);
int open_connection();
int sockfd;


int main()
{
    
    int sockfd = open_connection();
    while(1)
    {
        menu();
        char c = get_choice();
        switch (c)
        {
            case 'l':
            case 'L':
                list_files(sockfd);
                break;
            case 'd':
            case 'D':
                download_file(sockfd);
                break;
            case 'q':
            case 'Q':
                close_connection(sockfd);
                exit(0);
            default:
                printf("Please enter d, l, or q\n");
                break;
        }
    }
}

void menu()
{
    printf("L)ist files\n");
    printf("D)ownload a file\n");
    printf("Q)uit\n");
    printf("\n");
}

char get_choice()
{
    char *line = readline("Choice: ");
    char first = line[0];
    free(line);
    return first;
}

int open_connection()
{
    struct sockaddr_in sa;
    int sockfd;
    // gethostbyname
    struct hostent *he = gethostbyname("runwire.com");
    struct in_addr *ip = (struct in_addr*)he->h_addr_list[0];
    printf("IP addres is %s\n", inet_ntoa(*ip));
    
    // Fill in socket struct
    printf("creating struct\n");
    sa.sin_family = AF_INET;
    sa.sin_port = htons(1234);
    sa.sin_addr = *((struct in_addr *)ip);
    
    // Create socket
    printf("creating socket\n");
    sockfd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (sockfd == -1)
    {
        fprintf(stderr, "cant creat socket\n");
        exit(3);
    }
    // Connect
    printf("connecting\n");
    int res = connect (sockfd, (struct sockaddr *)&sa, sizeof(sa));
    if (res == -1)
    {
        fprintf(stderr, "cant connect\n");
        exit(2);
    }
    char buf[1000];
    // Get the responce
    int rsize = recv(sockfd, buf, 1000,0);
    printf("%s", buf);
    // return socket number (file descriptor)
    return sockfd;

}

void list_files(int s)
{
    // Send LIST command
    char list[1000];
    sprintf(list, "LIST\n");
    send(s, list, strlen(list),0);
    
    // Display to user
    recv(s, list, 1000, 0);
    printf("%s", list);
}
void download_file(int s)
{   
    
    
    // Ask user which file
    
    char fname[100];
    prompt:
    printf("Which file would you like?\n");
    scanf("%s", fname);
    
    // Find out how big the file is
    
    char buf[1000];
    char sizebuf[1000];
    sprintf(buf, "SIZE %s\n", fname);
    send(s, buf, strlen(buf),0);
    printf("sent: %s", buf);
    int r = recv(s, buf, 4,0);
    buf[r] = '\0';
    printf("received: %s\n", buf);
    int sr = recv(s, sizebuf, 1000,0); // sizebuf
    buf[sr] = '\0';
    printf("received: %s\n", sizebuf);
    memset(buf, 0, sizeof(buf));
    
    // Send GET command
    
    sprintf(buf, "GET %s\n", fname);
    send(s, buf, strlen(buf),0);
    printf("sent: %s", buf);
    memset(buf,0,sizeof(buf));
    recv(s,buf,4,0);
    printf("Rec: %s\n",buf);
    if (strncmp("-",buf,1) == 0)
    {
        recv(s,buf,1000,0);
        goto prompt;
    }
    // Receive data
    
    FILE* e = fopen(fname, "w");
    if (!e)
    {
        printf("File error, cant open");
        exit(1);
    }
    
    int size, rsize;
    size = atoi(sizebuf);
    char buf2[1000];
    while (size != 0)
    {
        rsize = recv(s, buf2, 1000, 0);
        fwrite(buf2, rsize, 1, e);
        size-=rsize;
        memset(buf2, 0, sizeof(buf2));
        
    }
    fclose(e);
   
}

void close_connection(int s)
{
    // send QUIT
    char Q[100];
    printf("quitting\n");
    sprintf(Q, "QUIT\n");
    send(s, Q, strlen(Q),0);
    
    // Close socket
    close(s);
}